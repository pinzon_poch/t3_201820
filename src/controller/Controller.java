package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller 
{
	//Atributos
	public static final String DIR_TRIPS = "Divvy_Trips_2017_Q3.csv";
	public static final String DIR_STATIONS = "Divvy_Stations_2017_Q3Q4.csv";

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations()
	{
		manager.loadStations(DIR_STATIONS);
	}
	
	public static void loadTrips()
	{
		manager.loadTrips(DIR_TRIPS);
	}
		
	public static IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return manager.customerNumberN(stationID, n);
	}
	
}
