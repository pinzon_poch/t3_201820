package model.logic;

import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.*;

public class DivvyTripsManager implements IDivvyTripsManager {
	private Stack<VOTrip> StackTrips;
	private Queue<VOStation> QueueStations;
	
	public void loadStations (String stationsFile) 
	{
		try
		{
			QueueStations = new Queue<VOStation>();
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
		    String[] linea = reader.readNext();
		    while((linea=reader.readNext())!=null)
		    {
		    	String s="";
		    	for(String sub : linea)
		    	{
		    		s+=sub+",";
		    	}
		    	
		    	VOStation novo = new VOStation(s);
		    	QueueStations.enqueue(novo);	
		    }  
		    reader.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void loadTrips (String tripsFile) 
	{
		try
		{
			StackTrips = new Stack<VOTrip>();
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
		    String[] linea = reader.readNext();
		    while((linea=reader.readNext())!=null)
		    {
		    	String s="";
		    	for(String sub : linea)
		    	{
		    		s+=sub+",";
		    	}
		    	
		    	VOTrip novo = new VOTrip(s);
		    	StackTrips.push(novo);	
		    }
		    		    
		    reader.close();
		    
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
		
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n)
	{
		DoublyLinkedlist<String> bicicletas = new DoublyLinkedlist();
		Iterator iter = StackTrips.iterator();
		int i = 0;
		while(iter.hasNext() && n > i)
		{
			VOTrip a = (VOTrip)iter.next();
			if(a.getBicycleID()==bicycleId)
			{
				Node<String> nodo = new Node(a.getToStation());
				bicicletas.add(nodo);
				i++;
			}
		}
		return bicicletas;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		// TODO Auto-generated method stub
		Iterator<VOTrip> viajes = StackTrips.iterator();
		Iterator<VOStation> staciones = QueueStations.iterator(); 
		String nombre="";
		boolean encontrado = false;
		int i = 0;
		while(staciones.hasNext() && encontrado == false)
		{
			VOStation a = staciones.next();
			if(a.darId() == stationID)
			{
				nombre=a.darName();
				encontrado=true;
			}
		}
		while(viajes.hasNext())
		{
			VOTrip b = viajes.next();
			if(b.getToStation().equals(nombre) && b.id() == n)
			{
				return b;
			}
		}
		return null;
	}	
}
