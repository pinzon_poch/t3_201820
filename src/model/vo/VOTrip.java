package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip 
{
	//Atributos
	/**
	 * El id del viaje
	 */
	private int id;
	/**
	 * La estaci�n de la cual salio 
	 */
	private String getFromStation;
	/**
	 * La estaci�n a la cual llego
	 */
	private String getToStation;
	
	private double getTripSeconds;
	
	private int bicycleID;
	
	//Constructor
	public VOTrip(String info)
	{
		String[] I = info.split(",");
		getTripSeconds = Double.parseDouble(I[4]);
		id=Integer.parseInt(I[0]);
		bicycleID=Integer.parseInt(I[3]);
		getFromStation=I[6];
		getToStation = I[8];
	}
	//M�todos
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return id;
	}	
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		return getFromStation;
	}	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		return getToStation;
	}
	public int getBicycleID()
	{
		return bicycleID;
	}
	public double getTripSeconds()
	{
		return getTripSeconds;
	}
}
