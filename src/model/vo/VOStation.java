package model.vo;

/**
 * representation of an station object
 * @author msi
 *
 */
public class VOStation 
{
	//Atributos
	/**
	 * El id de la estaci�n
	 */
	private int id;
	/**
	 * El nombre de la estaci�n
	 */
	private String name;
	/**
	 * El nombre de la ciudad
	 */
	private String city;
	/**
	 * La latitude a la cual se encuentra
	 */
	private double latitude;
	/**
	 * La longitud a la cual se encuentra
	 */
	private double longitude;
	/**
	 * La capacidad de la estaci�n
	 */
	private int capacity;
	/**
	 * La fecha en la cual s ehabilito la estaci�n
	 */
	private String onlineDate;
	//Constructor
	public VOStation(String info)
	{
		String[] i = info.split(",");
		id=Integer.parseInt(i[0]);
		name = i[1];
		city=i[2];
		latitude=Double.parseDouble(i[3]);
		longitude=Double.parseDouble(i[4]);
		capacity=Integer.parseInt(i[5]);
		onlineDate= i[6];
	}
	//M�todos
	/**
	 * da la ciudad de la estaci�n
	 * @return la ciudad
	 */
	public String darCity()
	{
		return city;
	}
	/**
	 * da el id de la estaci�n
	 * @return id
	 */
	public int darId()
	{
		return id;
	}
	/**
	 * da el nombre de la estacion
	 * @return name
	 */
	public String darName()
	{
		return name;
	}
	/**
	 * da la latitude a la cual se encuentra la estaci�n
	 * @return latitude
	 */
	public double darLatitude()
	{
		return latitude;
	}
	/**
	 * da la longitud a la cual se encuentra la ciudad
	 * @return longitude
	 */
	public double darLongitude()
	{
		return longitude;
	}
	/**
	 * da la capacidad que tiene la estaci�n
	 * @return capacity
	 */
	public int darCapacity()
	{
		return capacity;
	}
	/**
	 * da la fecha en la cual la estaci�n fue creada
	 * @return onlineDate
	 */
	public String darOnlineDate()
	{
		return onlineDate;
	}
}
