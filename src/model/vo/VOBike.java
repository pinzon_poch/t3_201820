package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike {
	//Atributos
	/**
	 * El id de la bicicleta
	 */
	private int id;
	//Constructor
	public VOBike(int pId)
	{
		id=pId;
	}
	//M�todos
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() 
	{
		return id;
	}	
}
