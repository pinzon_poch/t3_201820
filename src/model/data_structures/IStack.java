package model.data_structures;

import java.util.Iterator;

public interface IStack<T> extends Iterable<T>{
	
	/**
	 * Retorna true si la Pila esta vacia
	 * @return true si la Pila esta vacia, false de lo contrario
	 */
	public boolean isEmpty();
	
	/**
	 * Retorna el numero de elementos contenidos
	 * @return el numero de elemntos contenidos
	 */
	public int size();
	
		
	/**
	 * Quita y retorna el elemento agregado más recientemente
	 * @return el elemento agregado más recientemente
	 */
	public T pop();	
	/**
	 * La interfaz de ListIterator
	 */
	interface ListIterator<T> extends Iterator<T>
	{		
		public boolean hasNext();
		public T next();
	}
	/**
	 * el m�todo para agregar al stack
	 * @param t
	 * @param id
	 */
	void push(T t);

}
