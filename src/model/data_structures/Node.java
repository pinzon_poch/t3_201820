package model.data_structures;

public class Node<T> implements Comparable<Node<T>>
{	//Atributos
	/**
	 * el nodo anterio 
	 */
	private Node<T> anterior;
	/**
	 * el nodo que le sigue
	 */
	private Node<T> siguiente;
	
	/**
	 * el objeto contenido
	 */
	private T ob;
	
	//Constructor
	/**
	 * El contructor generico de nodo
	 * @param item el objeto a contener dentro del nodo
	 * @param pid el id del objeto a contener dentro dedl nodo
	 */
	public Node(T item)
	{
		anterior =null;
		siguiente = null;
		ob=item;
		
	}
	//M�todos
	/**
	 * Devuelve el objeto contenido
	 * @return ob
	 */
	public T darObjeto()
	{
		return ob;
	}
	/**
	 * cambia el siguiente nodo
	 * @param s el nodo que va a seguirle a this
	 */
	public void cambiarSiguiente(Node<T> s)
	{
		siguiente =s;
	}
	/**
	 * Cambia el nodo anterio a est
	 * @param s el nodo que va preseder
	 */
	public void cambiarAnterior(Node<T> s)
	{
		anterior=s;
	}
	/**
	 * devuelve el nodo que le sigue
	 * @return siguiente
	 */
	public Node<T> getSiguiente()
	{
		return siguiente;
	}
	/**
	 * devuelve el nodo que lo precede
	 * @return anterior
	 */
	public Node<T> darAnterior()
	{
		return anterior;
	}
	@Override
	public int compareTo(Node<T> arg0) {
		// TODO Auto-generated method stub
		return 0;
	}		
}
