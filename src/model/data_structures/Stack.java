package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>
{
	//Atriariables
	/**
	 * es el tama�o del stack
	 */
	private int size;
	/**
	 * el primer nodo que se en cuntra en el stack/ el nodo m�s reciente
	 */
	private Node<T> first;
	//Constructor
	public Stack()
	{
		size =0;
		first =null;
	}
	//M{etodost 
	/**
	 * Mira si el stack est� vacio 
	 * @return true si el stack est� vacio y flase si el stack no est� vacio
	 */
	@Override
	public boolean isEmpty() {
		return first==null;
	}
	/**
	 * devuelve el tam�o del stack
	 * @return el size del stack
	 */
	@Override
	public int size() {
		return size;
	}
	/**
	 * agrega un  nuevo objeto <T> al stack en el Nodo firts y aumenta el size por 1
	 * @param objeto T !=null
	 */
	@Override
	public void push(T t)
	{
		Node<T> nuevo = new Node<T>(t);
		if(first==null)
		{
			first=nuevo;
		}
		else
		{
			nuevo.cambiarSiguiente(first);
			first = nuevo;
			
		}
		size+=1;
	}
	/**
	 * elimina el elemnto en el nodo First y lo devuelve
	 * @return el elemnto que ha sido eliminado
	 */
	@Override
	public T pop() {
		T pop = first.darObjeto();
		first=first.getSiguiente();
		size--;
		return pop;
	}
	/**
	 * devuelve el iterador de la lista
	 * @return ListIterator
	 */
	@Override
	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	/**
	 * la clase listIterator del stack
	 * @author msi
	 *
	 */
	private class ListIterator implements Iterator<T>
	{	//Atributos
		Node<T> current =  first;
		//M�todos
		/**
		 * verifica que haya un objeto siguiente
		 * @return true si hay un objeto siguiente y false si no hay objeto
		 */
		@Override
		public boolean hasNext()
		{
			return current !=null;
		}
		/**
		 * Devuelve el siguiente objeto en el iterador
		 * @return el objeto que se encuentra en el nodo current
		 */
		@Override
		public T next() 
		{
			T next = (T)current.darObjeto();
			current= current.getSiguiente();
			return next;
		}
		
	}
	

}
