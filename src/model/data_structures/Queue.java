package model.data_structures;

import java.util.Iterator;


public class Queue<T> implements IQueue<T>
{
	// Atributos

	private int size;
	private Node<T> first;
	private Node<T> last;

	// Constuctor

	public Queue()
	{
		size = 0;
		first = null;
		last = null;
	}

	// Metodos

	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return new ListIterator();
	}

	@Override
	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return first == null;
	}

	@Override
	public int size() 
	{
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void enqueue(T objeto)
	{
		Node<T> oldlast = last;
		last = new Node<T>(objeto);
		last.cambiarAnterior(oldlast);
		last.cambiarSiguiente(null);
		if (isEmpty())
		{
			first = last;
		}
		else 
		{
			oldlast.cambiarSiguiente(last);
		}
		size++;
	}

	@Override
	public T dequeue() 
	{
		T item = first.darObjeto();
		first = first.getSiguiente();
		if (isEmpty())
		{
			last = null;
		}
		size--;
		return item;
	}

	private class ListIterator implements Iterator<T>
	{
		private Node<T> current = first;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			T object = (T) current.darObjeto();
			current = current.getSiguiente();
			return object;
		}
	}
}