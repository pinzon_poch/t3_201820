import static org.junit.Assert.*;

import org.junit.Test;
import model.vo.VOTrip;;

public class VOTripTest {

	@Test
	public void constructor() 
	{
		try
		{
			String s = "1010,null,null,1000,1.0,null,comienzo, null,final";
			VOTrip prueba=new VOTrip(s);
		}
		catch(Exception e)
		{ 
			fail("No se pudo contruir el objeto de tipo VOTrip");
		}
	}
	@Test
	public void infoInicial()
	{
		String s = "1010,null,null,1000,1.0,null,comienzo, null,final";
		VOTrip prueba=new VOTrip(s);
		assert(prueba.getFromStation().equals("comienzo")):"No almaceno correctamente la información de la estación de inicio";
		assert(prueba.getToStation().equals("final")):"No almaceno la esatación del final correctamente";
		assert(prueba.id()==1010):"No almaceno correctamente la información del id";
		assert(prueba.getTripSeconds()==1.0):"No guardo correctamente el tiempo";
	}

}
