import static org.junit.Assert.*;

import org.junit.Test;

import model.vo.VOStation;

public class VOStationTest {

	@Test
	public void constructor() 
	{
		try
		{
			String s = "1"+",prueba,"+"ciudad,"+"1.0,"+"2.0,"+"1,"+"10/10/2018";
			VOStation n = new VOStation(s);
		}
		catch(Exception e)
		{
			fail("No se pudo crear el objeto de tipo VOStation");
		}
	}
	@Test
	public void info()
	{
		String s = "1"+",prueba,"+"ciudad,"+"1.0,"+"2.0,"+"1,"+"10/10/2018";
		VOStation n = new VOStation(s);
		assert(n.darCapacity()==1):"La capacidad de la estaci�n no es la correcta";
		assert(n.darCity().equals("ciudad")):"No se almaceno correctamente la ciudad";
		assert(n.darId()==1):"El id es el incorrecto";
		assert(n.darName().equals("prueba")):"No se guardo correctamente el nombre de la estaci�n";
		assert(n.darOnlineDate().equals("10/10/2018")):"No se guardo correctamente el online date";
		assert(n.darLatitude()==1.0):"No se guardo correctamente la latitude de la estaci�n";
		assert(n.darLongitude()==2.0):"No se guardo correctamente la longitud de la estaci�n";
	}

}
