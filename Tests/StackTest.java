import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Stack;

public class StackTest {

	@Test
	public void constructor()
	{
		try
		{
			Stack<Object> prueba = new Stack<Object>();
		}
		catch(Exception e)
		{
			fail("no se pudo crear el Stack: "+e.getMessage());
		}
	}
	
	@Test
	public void infoInicial()
	{
		Stack<Object> prueba = new Stack<Object>(); 
		
		assert(prueba.size()==0):"El tama�o inicial del stack es el incorrecto: "+ prueba.size();
		assert(prueba.isEmpty()==true): "El stack deber�a estar vacio";
		assert(!prueba.iterator().hasNext()):"El iterador no deber�a tener siguiente";
	}
	
	@Test
	public void agregar()
	{
		Stack<Character> prueba = new Stack<Character>();
		
		char ob = 'p';
		
		try
		{
			prueba.push(ob);
		}
		catch(Exception e)
		{
			fail("no se pudo agregar el objeto nuevo al Stack");
		}
		
		assert(prueba.isEmpty()==false):"El stack sigue vacio";
		assert(prueba.size()==1):"el size del stack es el incorrecto";
		assert(prueba.iterator().hasNext()):"el iterador esta mal y no muestra un siguiente";
		assert(prueba.iterator().next()!=null):"el iterador esta mal y no muestra un siguiente objeto";
		
		try
		{
			
			if(prueba.pop()==null)
			{
				throw new Exception();
			}
			if(prueba.size()!=0)
			{
				throw new Exception();
			}			
			if(!prueba.isEmpty())
			{
				throw new Exception();
			}
		}
		catch(Exception e)
		{
			fail("no se puede hacer pop del objeto");
		}		
	}
	@Test
	public void crearIterador()
	{
		try{
			Stack<Character> prueba = new Stack<Character>();
			prueba.iterator();
		}
		catch(Exception e)
		{
			fail("fallo itterador");
		}
	}

}
